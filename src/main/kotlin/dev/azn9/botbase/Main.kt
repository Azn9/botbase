/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase

import com.github.philippheuer.events4j.simple.SimpleEventHandler
import com.github.twitch4j.TwitchClientBuilder
import dev.azn9.botbase.event.TwitchEventHandler
import dev.azn9.botbase.misc.*
import dev.azn9.botbase.services.CommandService
import dev.azn9.botbase.services.FileService
import dev.azn9.botbase.services.ReactionService
import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.event.domain.lifecycle.ReadyEvent
import discord4j.core.event.domain.message.MessageCreateEvent
import discord4j.core.event.domain.message.ReactionAddEvent
import discord4j.rest.util.Snowflake

lateinit var discordClient: DiscordClient
lateinit var localLastVideoId: String

fun main() {
    getEnvProperty(DISCORD_TOKEN).orElseThrow { IllegalStateException("Discord token is not provided") }.let { token -> discordClient = DiscordClientBuilder.create(token).build() }

    registerYoutubeListener()
    registerTwitchListener()

    discordClient.withGateway { gateway ->
        gateway.on(MessageCreateEvent::class.java).subscribe { event -> CommandService.processCommand(event).subscribe() }
        gateway.on(ReactionAddEvent::class.java).subscribe { event -> ReactionService.processReactionAdd(event).subscribe() }

        gateway.on(ReadyEvent::class.java).subscribe { ReactionService.registerSettingsParameters() }

        gateway.onDisconnect()
    }

    discordClient.login().block()
}

fun registerYoutubeListener() {
    getEnvProperty(YOUTUBE_TOKEN)
        .ifPresent { token ->
        FileService.readInFile(YOUTUBE_FILE_PATH).subscribe { content ->
            if (content.isNullOrEmpty())
                localLastVideoId = content
        }

        getEnvProperty(YOUTUBE_CHANNEL)
            .ifPresent { channel ->
            getEnvProperty(YOUTUBE_RANK)
                .ifPresent { rankId ->
                getEnvProperty(GUILD_ID)
                    .ifPresent { guildId ->
                    getEnvProperty(YOUTUBE_ALERT_CHANNEL)
                        .ifPresent { alertChannelId ->
                        val alertChannel = discordClient.getChannelById(Snowflake.of(alertChannelId))
                        val parameter =
                            getEmbedSettings(PARAMETER_YOUTUBE)
                        discordClient.getRoleById(Snowflake.of(guildId), Snowflake.of(rankId)).data.subscribe { roleData ->
                            val roleMention = "@${roleData.name()}"

                            YoutubeAPI.getLatestVideoId(channel, token).subscribe { videoId ->
                                if (videoId != localLastVideoId) {
                                    localLastVideoId = videoId
                                    FileService.writeInFile(videoId,
                                        YOUTUBE_FILE_PATH
                                    )

                                    if (!parameter.isEmpty()) {
                                        (alertChannel as MessageChannel).createMessage { messageSpec ->
                                            messageSpec.setContent(parameter.description.format(videoId))
                                        }.subscribe()

                                        (alertChannel as MessageChannel).createMessage { messageSpec ->
                                            messageSpec.setContent(roleMention)
                                        }.subscribe { message ->
                                            message.delete().subscribe()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

fun registerTwitchListener() {
    getEnvProperty(TWITCH_TOKEN)
        .ifPresent { token ->
        getEnvProperty(TWITCH_CHANNEL)
            .ifPresent { channel ->
            getEnvProperty(TWITCH_RANK)
                .ifPresent { rankId ->

                val twitchClient = TwitchClientBuilder.builder()
                        .withEnableHelix(true)
                        .withClientSecret(token)
                        .build()

                twitchClient.clientHelper.enableStreamEventListener(channel)

                getEnvProperty(GUILD_ID)
                    .ifPresent { guildId ->
                    getEnvProperty(TWITCH_ALERT_CHANNEL)
                        .ifPresent { alertChannelId ->
                        val alertChannel = discordClient.getChannelById(Snowflake.of(alertChannelId))
                        val parameter =
                            getEmbedSettings(PARAMETER_TWITCH)
                        discordClient.getRoleById(Snowflake.of(guildId), Snowflake.of(rankId)).data.subscribe { roleData ->
                            val roleMention = "@${roleData.name()}"

                            twitchClient.eventManager.getEventHandler(SimpleEventHandler::class.java).registerListener(
                                TwitchEventHandler(
                                    twitchClient,
                                    token
                                ) { title, game ->
                                    (alertChannel as MessageChannel).createEmbed { embedSpec ->
                                        embedSpec.setTitle(parameter.title)
                                        embedSpec.setDescription(parameter.description)
                                        embedSpec.setFooter(parameter.footer, parameter.footerIcon)
                                        embedSpec.addField("Titre", title, false)
                                        embedSpec.addField("Jeu", game, false)
                                    }.subscribe()

                                    (alertChannel as MessageChannel).createMessage { messageSpec ->
                                        messageSpec.setContent(roleMention)
                                    }.subscribe { message ->
                                        message.delete().subscribe()
                                    }
                                })
                        }
                    }
                }
            }
        }
    }
}
