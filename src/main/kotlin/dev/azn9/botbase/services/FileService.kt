/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase.services

import reactor.core.publisher.Mono
import java.time.Duration

class FileService {

    companion object {

        fun writeInFile(value: String, path: String) : Mono<Any?> {
            return Mono.create<Any?> { callback ->

            }.timeout(Duration.ofSeconds(5), Mono.empty())
        }

        fun readInFile(path: String) : Mono<String> {
            return Mono.create<String> { callback ->

            }.timeout(Duration.ofSeconds(5), Mono.empty())
        }

    }

}