/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase.services

import discord4j.core.event.domain.message.MessageCreateEvent
import reactor.core.publisher.Mono
import java.time.Duration

class CommandService {

    companion object {
        fun processCommand(event: MessageCreateEvent): Mono<Any?> {
            return Mono.create<Any?> { callback ->

            }.timeout(Duration.ofSeconds(5), Mono.empty())
        }
    }

}
