/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase.services

import dev.azn9.botbase.*
import dev.azn9.botbase.misc.*
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.event.domain.message.ReactionAddEvent
import discord4j.rest.util.Snowflake
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.util.function.Tuples
import java.time.Duration

class ReactionService {

    companion object {

        var settingMessages = HashMap<Long, SettingEmbedType>()

        fun processReactionAdd(event: ReactionAddEvent): Mono<Any?> {
            return Mono.create<Any?> { callback ->
                event.message.subscribe { message ->
                    when (settingMessages[message.id.asLong()]) {
                        SettingEmbedType.TWITCH -> {
                            getEnvProperty(TWITCH_RANK)
                                .ifPresent { rankId ->
                                event.member.ifPresent { member ->
                                    if (!member.roleIds.contains(Snowflake.of(rankId)))
                                        member.addRole(Snowflake.of(rankId)).subscribe(callback::success)
                                }
                            }
                        }
                        SettingEmbedType.YOUTUBE -> {
                            getEnvProperty(YOUTUBE_RANK)
                                .ifPresent { rankId ->
                                event.member.ifPresent { member ->
                                    if (!member.roleIds.contains(Snowflake.of(rankId)))
                                        member.addRole(Snowflake.of(rankId)).subscribe(callback::success)
                                }
                            }
                        }
                        SettingEmbedType.GAMES -> {
                            getEnvProperty(GAMES_RANK)
                                .ifPresent { rankId ->
                                event.member.ifPresent { member ->
                                    if (!member.roleIds.contains(Snowflake.of(rankId)))
                                        member.addRole(Snowflake.of(rankId)).subscribe(callback::success)
                                }
                            }
                        }
                        else -> {
                            //Not in map
                            callback.success()
                        }
                    }
                }
            }.timeout(Duration.ofSeconds(5), Mono.empty())
        }

        fun registerSettingsParameters() {
            getEnvProperty(PARAMETERS_CHANNEL)
                .ifPresent { channelId ->
                val channel = discordClient.getChannelById(Snowflake.of(channelId))

                channel.bulkDelete(channel.pinnedMessages.map { messageData -> messageData.id().toLong() }.publish()).doAfterTerminate {
                    Flux.fromArray(arrayOf(
                            Tuples.of(
                                getEmbedSettings(PARAMETER_TWITCH),
                                SettingEmbedType.TWITCH
                            ),
                            Tuples.of(
                                getEmbedSettings(PARAMETER_YOUTUBE),
                                SettingEmbedType.YOUTUBE
                            ),
                            Tuples.of(
                                getEmbedSettings(PARAMETER_GAMES),
                                SettingEmbedType.GAMES
                            )
                    )).filter { !it.t1.isEmpty() }.subscribe { tupleEmbedSettings ->
                        tupleEmbedSettings!!

                        val embedSettings = tupleEmbedSettings.t1

                        (channel as MessageChannel).createMessage { messageCreateSpec ->
                            messageCreateSpec.setEmbed { embedCreateSpec ->
                                if (embedSettings.title.isNotEmpty())
                                    embedCreateSpec.setTitle(embedSettings.title)

                                if (embedSettings.description.isNotEmpty())
                                    embedCreateSpec.setDescription(embedSettings.description)

                                if (embedSettings.footer.isNotEmpty())
                                    embedCreateSpec.setFooter(embedSettings.footer, embedSettings.footerIcon)
                            }
                        }.subscribe { message ->
                            settingMessages[message.id.asLong()] = tupleEmbedSettings.t2
                        }
                    }
                }.subscribe()
            }
        }
    }

}
