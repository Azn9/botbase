/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase.event

import com.github.philippheuer.events4j.simple.domain.EventSubscriber
import com.github.twitch4j.TwitchClient
import com.github.twitch4j.common.events.channel.ChannelGoLiveEvent
import com.github.twitch4j.helix.domain.Game

data class TwitchEventHandler(
        var twitchClient: TwitchClient,
        var token: String,
        var function: (String, String) -> Unit
) {

    @EventSubscriber
    fun onChannelGoLive(event: ChannelGoLiveEvent) {
        val game = twitchClient.helix.getGames(token, listOf(event.gameId), null).execute().games.stream().findFirst()
                .map { obj: Game -> obj.name }.orElse("")

        function.invoke(event.title, game)
    }

}
