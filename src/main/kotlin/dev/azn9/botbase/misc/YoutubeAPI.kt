/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase.misc

import JSon.JSONArray
import JSon.JSONObject
import reactor.core.publisher.Mono
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class YoutubeAPI {

    companion object {

    	const val YOUTUBE_API_URL = "https://www.googleapis.com/youtube/v3/search?channelId=%s&order=date&part=snippet&type=video&maxResults=1&key=%s"

        fun getLatestVideoId(channelId: String, apiKey: String): Mono<String> {
            return Mono.create { callback ->
                try {
                    var inputLine: String?
                    val url = URL(YOUTUBE_API_URL.format(channelId, apiKey))

                    val con = url.openConnection() as HttpURLConnection
                    con.requestMethod = "GET"

                    val `in` = BufferedReader(InputStreamReader(con.inputStream))

                    val line = StringBuilder()
                    while (`in`.readLine().also { inputLine = it } != null) line.append(inputLine)

                    `in`.close()

                    val jsonObject = JSONObject(line.toString())
                    con.disconnect()

                    if (!jsonObject.isNull("items")) {
                        val items: JSONArray = jsonObject.getJSONArray("items")

                        if (!items.isNull(0)) {
                            val item: JSONObject = items.getJSONObject(0)

                            if (!item.isNull("id")) {
                                callback.success(item.getJSONObject("id").getString("videoId"))
                            } else
                                callback.error(IllegalStateException("id field is null"))
                        } else
                            callback.error(IllegalStateException("item field does not contains elements"))
                    } else
                        callback.error(IllegalStateException("item field is null"))
                } catch (e: IOException) {
                    callback.error(e)
                }
            }
        }

    }
}