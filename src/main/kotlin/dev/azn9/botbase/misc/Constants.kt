/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase.misc

const val YOUTUBE_API_URL = "https://www.googleapis.com/youtube/v3/search?channelId=%s&order=date&part=snippet&type=video&maxResults=1&key=%s"

const val DISCORD_TOKEN = "DISCORD_TOKEN"
const val TWITCH_TOKEN = "TWITCH_TOKEN"
const val YOUTUBE_TOKEN = "YOUTUBE_TOKEN"
const val TWITCH_CHANNEL = "TWITCH_CHANNEL"
const val YOUTUBE_CHANNEL = "YOUTUBE_CHANNEL"

const val GUILD_ID = "GUILD_ID"
const val PARAMETERS_CHANNEL = "PARAMETERS_CHANNEL"
const val TWITCH_ALERT_CHANNEL = "TWITCH_ALERT_CHANNEL"
const val YOUTUBE_ALERT_CHANNEL = "YOUTUBE_ALERT_CHANNEL"

const val TWITCH_RANK = "TWITCH_RANK"
const val YOUTUBE_RANK = "YOUTUBE_RANK"
const val GAMES_RANK = "GAMES_RANK"

const val PARAMETER_TWITCH = "PARAMETER_TWITCH"
const val PARAMETER_YOUTUBE = "PARAMETER_YOUTUBE"
const val PARAMETER_GAMES = "PARAMETER_GAMES"

const val YOUTUBE_FILE_PATH = "/var/run/bot/youtube"