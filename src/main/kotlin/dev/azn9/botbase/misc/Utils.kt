/*
 * Copyright (c) 2020 - 2020 Axel Joly (Azn9).
 * Please contact me on contact@azn9.dev if you have any question.
 * You are not allowed to distribute or edit this source code without providing the current copyright header.
 */

package dev.azn9.botbase.misc

import JSon.JSONObject
import java.util.*

fun getEnvProperty(key: String): Optional<String> = Optional.ofNullable(System.getProperty(key))

fun getEmbedSettings(varName: String): SettingEmbed {
    getEnvProperty(varName).ifPresent { value ->
        val valueParsed = JSONObject(value)

        val settings = SettingEmbed()
        settings.title = valueParsed.getString("title") ?: ""
        settings.description = valueParsed.getString("description") ?: ""
        settings.footer = valueParsed.getString("footer") ?: ""
        settings.footerIcon = valueParsed.getString("footer_icon") ?: ""
    }

    return SettingEmbed()
}

class SettingEmbed {

    var title: String = ""
    var description: String = ""
    var footer: String = ""
    var footerIcon: String = ""

    fun isEmpty() : Boolean = this.title.isEmpty() && this.description.isEmpty() && this.footer.isEmpty() && this.footerIcon.isEmpty()

}

enum class SettingEmbedType {

    TWITCH,
    YOUTUBE,
    GAMES

}